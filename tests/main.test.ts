import request from 'supertest'

import { application } from '../src/main'

describe('app.ts', () => {
  describe('/', () => {
    it('should return 200 with html content', async () => {
      const response = await request(application()).get('/')
      expect(response.status).toBe(200)
      expect(response.header['content-type']).toBe('text/html; charset=utf-8')
    })
  })
})
