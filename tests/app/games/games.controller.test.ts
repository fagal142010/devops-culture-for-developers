import request from 'supertest'

import { application } from '../../../src/main'

describe('games.controller.ts', () => {
  describe('/api/v1/games', () => {
    it('should return 200 with json content', async () => {
      const response = await request(application()).get('/api/v1/games')
      expect(response.status).toBe(200)
      expect(response.header['content-type']).toBe('application/json; charset=utf-8')
    })
  })
})
