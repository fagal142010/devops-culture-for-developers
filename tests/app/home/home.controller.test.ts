import request from 'supertest'

import { application } from '../../../src/main'

describe('home.controller.ts', () => {
  describe('/api/v1', () => {
    it('should return 200 with html content', async () => {
      const response = await request(application()).get('/api/v1')
      expect(response.status).toBe(200)
      expect(response.header['content-type']).toBe('text/html; charset=utf-8')
    })
  })
})
