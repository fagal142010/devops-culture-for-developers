import express, { Request, Response } from 'express'
import { wrap } from 'async-middleware'

import { createLogger } from '../../logger'
import * as GameService from './games.service'

const logger = createLogger('games-controller')

export const gameRoutes = express.Router()
gameRoutes.get('/', wrap(getAll))
gameRoutes.get('/:id', wrap(getBy))
gameRoutes.put('/:id', wrap(updateAll))
gameRoutes.patch('/:id', wrap(update))
gameRoutes.delete('/:id', wrap(remove))
gameRoutes.post('/', wrap(create))

export async function getAll(req: Request, res: Response) {
    res.send({ data: await GameService.getAll() })
}

export async function getBy(req: Request, res: Response) {
    res.send({ data: await GameService.getBy(req.params.id) })
}

export async function create(req: Request, res: Response) {
    logger.info('Event has passed through route')
    res.send({ data: await GameService.create(req.body) })
}

export async function updateAll(req: Request, res: Response) {
    res.send({ data: await GameService.update(req.body) })
}

export async function update(req: Request, res: Response) {
    res.send({ data: await GameService.update(req.body) })
}

export async function remove(req: Request, res: Response) {
    res.send({ data: await GameService.remove(req.params.id) })
}
