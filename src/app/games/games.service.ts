import * as GameDal from './games.dal'

export function getBy(id: string): GameDal.Game {
    return GameDal.getBy(id)
}

export function getAll(): GameDal.Game[] {
    return GameDal.getAll()
}

export function create(game: GameDal.Game): GameDal.Game {
    return GameDal.create(game)
}

export function update(game: GameDal.Game): GameDal.Game {
    return GameDal.update(game)
}

export function remove(id: string): GameDal.Game {
    return GameDal.remove(id)
}
