import express, { Request, Response } from 'express'
import path from 'path'

export const homeRoutes = express.Router()

export const content = `
<div class="container">
  <div class="message">
    <h1>DevOps</h1>
    <hr style="border: 1px solid #066f09ed; margin: auto;">
    <p>Application Programming Interface 1.0</p>
  </div>
  <div class="author">by Gary Ascuy</div>
</div>
`

export const html = `
<!DOCTYPE html>
<html>
<title>DevOps API</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<head>
<style>
  body,h1 { font-family: "Raleway", sans-serif; }
  body, html { height: 100vh; overflow: hidden; }
  body {
    background: rgb(9,9,121);
    background: linear-gradient(180deg, rgba(9, 9, 121, 0.14) 2%, rgba(0,127,255,0.5536589635854341) 55%);
  }
  .container {
    display: flex;
    align-items: center;
    justify-content: center;
    height: 100%;
  }
  .message {
    text-align: center;
    max-width: 50%;
  }
  .author {
    position: fixed;
    right: 30px;
    bottom: 30px;
    color: #c50c2d;
    font-weight: bold;
  }
</style>
</head>
<body>${content}</body>
</html>
`

homeRoutes.get('/favicon.ico', (req: Request, res: Response) => res.sendFile(path.resolve('./src/assets/favicon.png')))
homeRoutes.get('/favicon.png', (req: Request, res: Response) => res.sendFile(path.resolve('./src/assets/favicon.png')))

homeRoutes.use('/', (req: Request, res: Response) => res.send(html))
homeRoutes.use('/api', (req: Request, res: Response) => res.send(html))
homeRoutes.use('/api/v1', (req: Request, res: Response) => res.send(html))