import nconf from 'nconf'

nconf.argv()
  .env('__')
  .file({ file: './src/settings/defaults.json' })

export function get(key: string) {
  return nconf.get(key)
}

export const settings = nconf
