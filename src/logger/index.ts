import winston from 'winston'

import { get } from '../settings'

export function createLogger(service: string) {
  return winston.createLogger({
    level: get('logger:level'),
    defaultMeta: { service },
    transports: [
      new winston.transports.Console({
        format: winston.format.combine(
          winston.format.colorize(),
          winston.format.simple()
        ),
      })
    ],
  })
}
