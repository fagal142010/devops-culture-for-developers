import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import express from 'express'

import { get } from './settings'
import { createLogger } from './logger'

import { errorHandler } from './middlewares'
import { controllers } from './app'

const logger = createLogger('api')

export function application() {
  const app = express()

  app.use(cors())
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(cookieParser())
  
  app.use(controllers)
  app.use(errorHandler)
  return app
}

export async function main() {
  try {
    const app = application()
    const { port, host } = get('service')
    const server = app.listen(port, host, () => logger.info(`server running at ${host}:${port}`))
    return server
  } catch (error) {
    logger.error('Error trying to start application', error)
  }
}
