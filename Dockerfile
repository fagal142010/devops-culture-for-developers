
# Stage 1 - TypeScript Build
FROM node:14.4-alpine as builder
LABEL maintainer="Gary Ascuy <gary.ascuy@gmail.com>"

ENV NODE_ENV=development
WORKDIR /node/app
COPY . .
RUN yarn install --no-cache && yarn build && yarn clean:production

# Stage 2 - NodeJS Image
FROM node:14.4-alpine
LABEL maintainer="Gary Ascuy <gary.ascuy@gmail.com>"

ENV name=devops NODE_ENV=production \
    server__host=0.0.0.0 server__port=3666 \
    logger__level=info
WORKDIR /node/app
COPY --from=builder /node/app .
RUN yarn install --production --no-cache

EXPOSE 3666
CMD yarn start:production
